package hairdressing.academy.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class StudentTest {

    @Test
    public void TestToString() {
        final Student student = new Student("Pedro Picapiedra", "50123123");
        assertEquals(student.getFullName(), "Pedro Picapiedra");
        assertEquals(student.getIdDocument(), "50123123");
        assertEquals(student.toString(), "{ fullName: Pedro Picapiedra, idDocument: 50123123}");
    }

    //TODO agregar test que validen los limites y excepciones.
    //TODO crear clases de test para las demas entidades.
}
