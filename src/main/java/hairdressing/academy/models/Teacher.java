package hairdressing.academy.models;

public class Teacher {

    //TODO ver que concepto podemos aplicar aqui para no repetir fullName y idDocument como en Student.
    private String fullName;
    private String idDocument;

    public Teacher(String fullName, String idDocument) {
        this.fullName = fullName;
        this.idDocument = idDocument;
    }

    @Override
    public String toString() {
        return "{ fullName: " + this.fullName + ", idDocument: " + this.idDocument + "}";
    }

    public String getFullName() {
        return fullName;
    }

    public String getIdDocument() {
        return idDocument;
    }
}
