package hairdressing.academy.models;

public class Schedule {

    private DayOfWeek dayOfWeek;
    private int initHour;
    private int endHour;

    public Schedule(DayOfWeek dayOfWeek, int initHour, int endHour) {
        //TODO debemos agregar validaciones por ejemplo el initHour no puede ser mayor al endHour
        //TODO otra validacion la hora de inicio y fin no pueden estar fuera del horario que el negociio se encuentra cerrado.
        //TODO otra validacino el dia de la semana no puede seer uno que no sea correspondiente al dia que esta cerrado. Por ejemplo
        // los dias domingos.
        this.dayOfWeek = dayOfWeek;
        this.initHour = initHour;
        this.endHour = endHour;
    }


    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public int getInitHour() {
        return initHour;
    }

    public int getEndHour() {
        return endHour;
    }
}
