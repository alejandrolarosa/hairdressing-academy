package hairdressing.academy.models;

public class Registration {

    private Course course;
    private Student student;

    public Registration(Course course, Student student) {
        //TODO validar aqui el maximo de alumnos anotados en el curso. si no lanzar una exception
        this.course = course;
        this.student = student;
    }

}
