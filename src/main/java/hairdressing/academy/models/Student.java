package hairdressing.academy.models;

public class Student {

    private String fullName;
    private String idDocument;

    public Student(String fullName, String idDocument) {
        this.fullName = fullName;
        this.idDocument = idDocument;
    }


    @Override
    public String toString() {
        return "{ fullName: " + this.fullName + ", idDocument: " + this.idDocument + "}";
    }

    public String getFullName() {
        return fullName;
    }

    public String getIdDocument() {
        return idDocument;
    }

}
