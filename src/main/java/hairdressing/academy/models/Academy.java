package hairdressing.academy.models;

import java.util.ArrayList;
import java.util.List;

//TODO notar que aqui se puede usar el pattern singleton
public class Academy {

    private String name;
    private List<DayOfWeek> dayOfWeeksWorking= new ArrayList<>();
    private int openingHour= 8;//default 8 hs
    private int closingHour= 23;//default 23 hs.
    private List<Course> courses = new ArrayList<>();
    private List<Teacher> teachers = new ArrayList<>();
    private List<Student> students = new ArrayList<>();
    private List<Registration> registrations = new ArrayList<>();


    public Academy(String name, List<DayOfWeek> dayOfWeeksWorking, int openingHour, int closingHour) {
        this.name = name;
        this.dayOfWeeksWorking = dayOfWeeksWorking;
        this.openingHour = openingHour;
        this.closingHour = closingHour;
    }

    public void addTeacher(Teacher teacher){
        //TODO validar que no se pueda agregar uno que ya exista, si ya existe lanzar una exception
     this.teachers.add(teacher);
    }

    public void addStudent(Student student){
        //TODO validar que no se pueda agregar uno que ya exista, si ya existe lanzar una exception
        this.students.add(student);
    }

    public void addRegistration(Registration registration){
        //TODO validar que no se pueda agregar uno que ya exista, si ya existe lanzar una exception
        this.registrations.add(registration);
    }

    public void addCourses(Course course){
        //TODO validar que no se pueda agregar uno que ya exista, si ya existe lanzar una exception
        this.courses.add(course);
    }
}
