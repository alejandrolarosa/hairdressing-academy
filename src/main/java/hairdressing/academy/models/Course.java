package hairdressing.academy.models;

public class Course {

    private String code;
    private String name;
    private String description;
    private int maximumOfStudents= 5; //valor por default
    private int year;
    private int quarter;
    private Schedule schedule;
    private Teacher teacher;

    public Course(String code, String name, String description, Schedule schedule, int year, int quarter, int maximumOfStudents) {
        //TODO validar year y quarter (1..4)
        //TODO ver como validar los horarios maximo y mimimo con el limite ingresado en Academy. openingHour, closingHour.
        this.code = code;
        this.name = name;
        this.description = description;
        this.schedule = schedule;
        this.year = year;
        this.quarter = quarter;
        this.maximumOfStudents = maximumOfStudents;
    }

    @Override
    public String toString() {
        return "{ code: " + this.code + ", name: " + this.name + " , description: " + this.description + " }";
    }

    public void addTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
}
