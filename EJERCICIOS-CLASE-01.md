# EJERCICIOS CLASE 01

Enunciado:

Lo que tenemos modelado, tiene que ver con la organizacion de un instituto
que da cursos, y requiere controlar lo siguiente en su alumnado
* Cuantos alumnos se encuentran anotados en un curso.
* Tener control sobre el limite de alumnos que pueden inscribirse en los cursos.
* Que los alumnos al inscribirse no lo hagan de manera repetiida en el curso, es decir
solo se puedan inscribir una sola vez en el curso.
* Que el sistema no permita dar de alta 2 veces al mismo profesor y al mismo alumno.
* Que no se puedan repetir los dias y horarios de cursos.
* Que los horarios se encuentren dentro de los horarios y dias que el 
instituto se encuentra abierto.
* Que no pueda anotarse a un curso un profesor. Y que un alumno no pueda ser profesor del curso.
* Obtener un listados de los cursos que tiene un profesor.
* Obtener un listado de alumnos de un curso particular.

# Modelado
Una vez obtenido el modelo que cubre la lista de funcionalidades y reglas de negocio
mencionadas anteriormente, deberiamos mejorar el codigo actual. Y proporcionar un listado
de cualidades del software que estamos teniendo en cuenta en la solucion.

# Patrones de diseno
Deberiamos identificar los patrones de diseno candidatos que estan presentes en la solucion.
